<?php
switch($_SERVER['REQUEST_URI']) {
    case "/version" :
        $return = array("result_code" => 200, "Name" => "Single AppID API", "version" => "1.1.0", "release date" => "January 26, 2019", "powered by" => "Streaming Application, TDG.", "signature" => md5(gethostname()));
        print(json_encode($return));
    break;
    
    ### SINGLE APPID ###
    case "/v3/livestream" :
        require_once __DIR__ .'/spapi/Livestream.php';
        $return = Livestream::provide();
        header("Content-Type: application/json");
        print(json_encode($return));
    break;
    case "/v3/vodstream" :
        require_once __DIR__ .'/spapi/Vodstream.php';
        $return = Vodstream::provide();
        header("Content-Type: application/json");
        print(json_encode($return));
    break;

    ### OLD API ###
    case '/streamingprovider' :
        require_once __DIR__ .'/spmiddleware/Streamingprovider.php';
        $return = Streamingprovider::provide($request, $response);
        header("Content-Type: application/json");
        print(json_encode($return));
    break;
    case '/streamingproviderv2' :
        require_once __DIR__ .'/spmiddleware/Streamingprovider2.php';
        $return = Streamingprovider2::provide($request, $response);
        header("Content-Type: application/json");
        $return['version'] = "3.0";
        print(json_encode($return));
    break;
    case '/vodstreamprovider' :
        require_once __DIR__ .'/spmiddleware/Vodstreamprovider.php';
        $return = Vodstreamprovider::provide($request, $response);
        header("Content-Type: application/json");
        $return['version'] = "3.0";
        print(json_encode($return));
    break;
    case '/tvodstreamprovider' :
        require_once __DIR__ .'/spmiddleware/Tvodstreamprovider.php';
        $return = Tvodstreamprovider::provide($request, $response);
        header("Content-Type: application/json");
        $return['version'] = "3.0";
        print(json_encode($return));
    break;
    
    default :
        header("HTTP/1.0 404 Not Found");
    break;
}