<?
class Base62Encoder {
	public static function encode ($data) {
		$outstring = '';
		$len = strlen($data);
		for ($i = 0; $i < $len; $i += 8) {
			$chunk = substr($data, $i, 8);
			$outlen = ceil((strlen($chunk) * 8) / 6);
			$x = bin2hex($chunk);
			$number = ltrim($x, '0');
			if ($number === '') $number = '0';
			$w = gmp_strval(gmp_init($number, 16), 62);
			$pad = str_pad($w, $outlen, '0', STR_PAD_LEFT);
			$outstring .= $pad;
		}
		return $outstring;
	}
	
	public static function decode ($data) {
		$outstring = '';
		$len = strlen($data);
		for ($i = 0; $i < $len; $i += 11) {
			$chunk = substr($data, $i, 11);
			$outlen = floor((strlen($chunk) * 6) / 8);
			$number = ltrim($chunk, '0');
			if ($number === '') $number = '0';
			$y = gmp_strval(gmp_init($number, 62), 16);
			$pad = str_pad($y, $outlen * 2, '0', STR_PAD_LEFT);
			$outstring .= pack('H*', $pad);
		}
		return $outstring;
	}
}