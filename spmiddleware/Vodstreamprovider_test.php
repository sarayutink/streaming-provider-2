<?
require_once 'Vodstreamprovider.req';

class Vodstreamprovider {
	public static function provide ($request, $response) {
		$body = $request->getBody();
		$GLOBALS['ctrl_name'] = "Vodstreamprovider";
		$GLOBALS['json_object'] = json_decode($body);
		unset($body);
		
		## validate post json
		if (Variable::validate()) {
			$apconf = ucfirst($GLOBALS['json_object']->appid)."config";
			require_once($GLOBALS['src_dir'] ."/configures/vodstreamprovider/".$apconf.".php");
			$GLOBALS['bizconf'] = new $apconf();
			## check ccu control
			// require_once($GLOBALS['src_dir'] ."/controllers/Ccucontrol.php");
			// if (Ccucontrol::check()) {
			if (true) {
				## get streaming server via load balancer
				require_once($GLOBALS['src_dir'] ."/controllers/Loadbalancecontrol.php");
				$group = $GLOBALS['json_object']->appid != "skyclip" ? 101 : $GLOBALS['bizconf']->getGroupId($GLOBALS['json_object']);
				$server = Loadbalancecontrol::findServer("http://172.22.160.29:8080/lalaynya.php", $group);
				if (!is_null($server) && $server != "406") {
					require_once($GLOBALS['src_dir'] ."/controllers/Streamcontrol.php");
					$action = Streamcontrol::createStreamPath();
					if(!is_null($action)) {
						$return = array('result_code' => 200, 'result' => "http://".$server.$action);
						Logger::writelog(array('result_code' => 200, 'result' => "http://".$server.$action));
					}
					else $return = array('result_code' => 430, 'result' => "Cannot find playlist.");
				}
				elseif ($server == "406") $return = array('result_code' => 200, 'result' => "406");
				else $return = array('result_code' => 420, 'result' => "Cannot find streaming server.");
			}
			else $return = array('result_code' => 410, 'result' => "Your concurrent reaches limit.");
		}
		else $return = array('result_code' => 600, 'result' => "Invalid request.");
		
		
		return $return;
    }
}