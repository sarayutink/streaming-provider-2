<?
function signQueryString ($smil) {
	$secret = "ǝɯƖ┴ǝɔ@dSln0SʎʇƖl@ǝɹɹǝʍ0ԀpuƖW";
	$sid = "503260f736";
	// $sid = bin2hex(openssl_random_pseudo_bytes(5));
	$rt = "1608198029";
	// $rt = "".time();

	$payload = array($sid => array(
		$GLOBALS['json_object']['appid'], 
		$smil,
		$GLOBALS['json_object']['type'],
		$GLOBALS['json_object']['visitor'],
		$GLOBALS['json_object']['uid'],
		str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']['sessionid'])),
		$rt	
	));
	echo "payload : " . json_encode($payload) ."\n";
	$payloadKey = hash_hmac("sha256", $secret, $sid);
	echo "payloadKey : " . $payloadKey ."\n";
	
	$token = str_replace(array("+", "/", "="), array("-", "_", ""), base64_encode(hash_hmac("sha256", json_encode($payload), $payloadKey, true)));

	return $token;
}

$json = '
{
    "uid": "87948446",
    "sessionid": "8d11e1300e",
    "appid": "antv2",
    "streamname": "1234567",
    "langid": "th",
    "streamlvl": "auto",
    "csip": "127.0.0.1",
    "agent": "Postman",
    "visitor": "mobile",
	"drm": "aes",
	"type":"live"
}
';

$smil = "ht111_th_tv_auto_smarttv.smil";
$GLOBALS['json_object'] = json_decode($json, true);

echo "token : " . (signQueryString($smil)) ."\n";;