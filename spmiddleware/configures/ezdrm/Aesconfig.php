<?
class Aesconfig {
	public static function generateManifest () {
		$streamname = "";
		// if (is_numeric($GLOBALS['json_object']->streamname)) {
		if (strpos($GLOBALS['json_object']->appid, "clip") === false) {
			// $streamname = ($GLOBALS['json_object']->visitor == "stb") ? "_stb_auto.smil" : "_m_auto.smil";
			$streamname = "_m_auto.smil";
			if (is_numeric($GLOBALS['json_object']->streamname)) $streamname = str_pad($GLOBALS['json_object']->streamname, 12, "0", STR_PAD_LEFT) . $streamname;
			else $streamname = $GLOBALS['json_object']->streamname . $streamname;
		}
		else {
			$streamname = $GLOBALS['json_object']->streamname;
		}
		$GLOBALS['signature'] = self::signQueryString($streamname);
		
		return isset($GLOBALS['bizconf']->bizconf->aes->appinst) ? "/". $GLOBALS['bizconf']->bizconf->aes->appinst ."/". $streamname ."/playlist.m3u8?appid=". $GLOBALS['json_object']->appid ."&uid=". $GLOBALS['json_object']->uid ."&visitor=". $GLOBALS['json_object']->visitor : null;
	}
	
	public static function generateLicense () {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		$str2decrypt = time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". explode("_", $GLOBALS['json_object']->streamname)[0] ."|". $GLOBALS['json_object']->uid ."|unlimit";
		$encrypt = Opensslcryption::encrypt($str2decrypt);
		$querystring = "&mpass={$encrypt}".$GLOBALS['signature'];
		
		return $querystring;
	}
	
	public static function generateBypassLicense () {
		$encrypt = Opensslcryption::encryptbypass();
		$querystring = "?appid={$GLOBALS['json_object']->appid}&uid={$GLOBALS['json_object']->uid}&mpass={$encrypt}".$GLOBALS['signature'];
		
		return $querystring;
	}
	
	private static function signQueryString ($smil) {
		$secret = "ǝɯƖ┴ǝɔ@dSln0SʎʇƖl@ǝɹɹǝʍ0ԀpuƖW";
        $sid = bin2hex(openssl_random_pseudo_bytes(5));
		$rt = "".time();
		$type = (strpos($GLOBALS['json_object']->appid, "svod") !== false) ? "svod" : "clip";
	
		$payload = array($sid => array(
			$GLOBALS['json_object']->appid, 
			$smil,
			$type,
			$GLOBALS['json_object']->visitor,
			$GLOBALS['json_object']->uid,
			str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']->sessionid)),
			$rt	
		));
		
		$payloadKey = hash_hmac("sha256", $secret, $sid);
		
		$token = str_replace(array("+", "/", "="), array("-", "_", ""), base64_encode(hash_hmac("sha256", json_encode($payload), $payloadKey, true)));
	
		return "&sid={$sid}&rt={$rt}&tk={$token}";
	}
}