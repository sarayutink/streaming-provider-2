<?
class Streamconfig {
	var $bizctrl;
	var $bizconf;
	var $appdb;
	var $confdb = 0;
	var $chdb = 1;
	
	public function __construct () {
		$this->bizctrl = $this->loadRedisXML();
		$this->bizconf = $this->loadRedisConfig();
		$this->appdb = $this->getAppDB();
	}
	
	// private function loadBizConf ($appname, $path = APPPATH) {
		// if (file_exists($path.$appname)) {
			// /** load config in XML */
			// try {
				// return simplexml_load_file($path.$appname);
			// }
			// catch(Exception $e) {
				// return false;
			// }
		// }
		// else {
			// return false;
		// }
	// }
	
	private function loadRedisXML () {
		$xml_str = "<?xml version='1.0' encoding='UTF-8'?><configure>";
		$xml_str .= '<streammapping>'. $GLOBALS['redis']->getRedis($GLOBALS['json_object']->channelid, 1) .'</streammapping>';
		$xml_str .= '</configure>';
		
		return simplexml_load_string($xml_str);
	}
	
	private function loadRedisConfig () {
		$json_str = $GLOBALS['redis']->getRedis($GLOBALS['json_object']->appid .":config", 0);
		
		return json_decode($json_str);
	}
	
	private function getAppDB () {
		$serial = $GLOBALS['redis']->getRedis($GLOBALS['json_object']->appid .":db", 0);
		
		return unserialize($serial);
	}
	
	private function loadLocalXML ($filepath) {
		if (file_exists($filepath)) {
			/** load config in XML */
			try {
				return simplexml_load_file($filepath);
			}
			catch(Exception $e) {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public function isValidChannelID () {
		$type = $GLOBALS['json_object']->type;
		return in_array($GLOBALS['json_object']->channelid, $this->bizconf->$type);
	}
	
	public function generatePlaylist () {
		// echo $object->programstate;
		$visitor = (preg_match("/^(lwweb|web|mobile)$/", @$GLOBALS['json_object']->visitor)) ? "mobile" : $GLOBALS['json_object']->visitor;
		require_once $GLOBALS['src_dir'] .'/configures/Blackoutconfig.php';
		$isBlackout = Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid);
		$query = !$isBlackout ? implode(";", array($GLOBALS['json_object']->appid, $GLOBALS['json_object']->langid, $GLOBALS['json_object']->type, $GLOBALS['json_object']->visitor, $GLOBALS['json_object']->streamlvl, "aes", $GLOBALS['json_object']->channelid)) : implode(";", array($GLOBALS['json_object']->appid, $GLOBALS['json_object']->langid, $GLOBALS['json_object']->type, $GLOBALS['json_object']->visitor, $GLOBALS['json_object']->streamlvl, "aes", "bk"));
		$default = !$isBlackout ? implode(";", array("htv", $GLOBALS['json_object']->langid, $GLOBALS['json_object']->type, $visitor, $GLOBALS['json_object']->streamlvl, "aes", $GLOBALS['json_object']->channelid)) : implode(";", array("htv", $GLOBALS['json_object']->langid, $GLOBALS['json_object']->type, $visitor, $GLOBALS['json_object']->streamlvl, "aes", "bk"));

		$return = $GLOBALS['redis']->getRedis($query, 1, array('host' => '10.18.19.98', 'port' => 6380));
		if ($return !== false) {
			$return = str_replace("yyyymmdd_HHmmss", date("Ymd_His", (int)@$GLOBALS['json_object']->stime), $return);
			// file_put_contents("/www/logs/generatePlaylist.sp2.log", $default ." >> ".$return."\n",FILE_APPEND);
			$GLOBALS['signature'] = $this->signQueryString(explode("/", $return)[1]);
			return "/".$return."/playlist.m3u8";
		}
		else {
			$return = $GLOBALS['redis']->getRedis($default, 1, array('host' => '10.18.19.98', 'port' => 6380));
			$return = str_replace("yyyymmdd_HHmmss", date("Ymd_His", (int)@$GLOBALS['json_object']->stime), $return);
			// file_put_contents("/www/logs/generatePlaylist.sp2.log", $default ." >> ".$return."\n",FILE_APPEND);
			$GLOBALS['signature'] = $this->signQueryString(explode("/", $return)[1]);
			return "/".$return."/playlist.m3u8";
		}
	}
	
	public function getGroupId () {
		$visitor = ($GLOBALS['json_object']->visitor == "web" ? "mobile" : $GLOBALS['json_object']->visitor);
		$query = '//streamprofile/language[@id="'. strtolower($GLOBALS['json_object']->langid) .'"]/profile[@streamlevel="'. $GLOBALS['json_object']->streamlvl .'" and @device="'. $visitor .'" and @type="'. strtolower($GLOBALS['json_object']->type) .'"]';
		$ret_arr = $this->bizctrl->xpath($query);
		// var_dump($query);
		if (count($ret_arr) > 0) return (string)$ret_arr[0]->group;
		else return null;
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
	}
	
	public function getrsaqstring ($is_blackout = false) {
		require_once $GLOBALS['src_dir'] .'/libraries/Opensslcryption.php';
		//require_once $GLOBALS['src_dir'] .'/libraries/ArsOpensslcryption.php';
		$token_arr = array('oqxg55CptqALr0','aFEacfYcpOtkGi','famnCWFwjvlksk','oP9Qlqas8YJlvp','9UxURr9saLEd5F');
		$channelid = ($GLOBALS['json_object']->channelid != '021') ? $GLOBALS['json_object']->channelid : '135';
		$ch_list = "o028";
		$stream = !Blackoutconfig::isBlackout($GLOBALS['json_object']->appid, $GLOBALS['json_object']->channelid) ? $channelid : "bk";
		
		$encrypt = Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']->sessionid ."|". $GLOBALS['json_object']->appid ."|". $GLOBALS['json_object']->csip ."|". $stream ."|". $GLOBALS['json_object']->uid ."|". @$dvr);
		
		if ($GLOBALS['json_object']->type == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']->type == "catchup") @$querystring = "wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']->stime, "YmdHis")}&wowzadvrplaylistduration={$GLOBALS['json_object']->duration}&";
		@$querystring .= "appid={$GLOBALS['json_object']->appid}&visitor={$GLOBALS['json_object']->visitor}&uid={$GLOBALS['json_object']->uid}&did=". str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']->sessionid)) ."&mpass={$encrypt}". $GLOBALS['signature'];
		
		return $querystring;
	}
	
	public function signQueryString ($smil) {
		$secret = "ǝɯƖ┴ǝɔ@dSln0SʎʇƖl@ǝɹɹǝʍ0ԀpuƖW";
        $sid = bin2hex(openssl_random_pseudo_bytes(5));
		$rt = "".time();
		$type = $GLOBALS['json_object']->type;
	
		$payload = array($sid => array(
			$GLOBALS['json_object']->appid, 
			$smil,
			$type,
			$GLOBALS['json_object']->visitor,
			"".$GLOBALS['json_object']->uid,
			str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']->sessionid)),
			$rt	
		));
		$payloadKey = hash_hmac("sha256", $secret, $sid);
		
		$token = str_replace(array("+", "/", "="), array("-", "_", ""), base64_encode(hash_hmac("sha256", json_encode($payload), $payloadKey, true)));
	
		return "&type=$type&sid={$sid}&rt={$rt}&tk={$token}";
	}
}
