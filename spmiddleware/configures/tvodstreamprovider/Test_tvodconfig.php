<?
include_once( dirname(__DIR__). '/Tvodconfig.php');
class Test_tvodconfig extends Tvodconfig {
	public function __construct () {
		parent::__construct();
	}
	
	public function getLicense () {
		switch ($GLOBALS['json_object']->drm) {
			case "wv":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Wvconfig.php";
				return Wvconfig::generateBypassLicense();
			break;
			case "fp":
				require_once $GLOBALS['src_dir'] ."/configures/ezdrm/Fpconfig.php";
				return Fpconfig::generateBypassLicense();
			break;
			default:
				return null;
			break;
		}
	}
}
?>