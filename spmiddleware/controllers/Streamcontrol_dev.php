<?
class Streamcontrol_dev {
	
	public static function createStreamPath() {		
		$plurl = $GLOBALS['bizconf']->generatePlaylist();
		if (!empty($plurl)) {
			$qstring = $GLOBALS['bizconf']->getrsaqstring();
			
			return "$plurl?$qstring";
		}
		else {
			Logger::writelog(array('result_code' => 431, 'result' => 'LIVE_MAPPING_CONFLICT'));
			return null;
		}
	}
	
	public static function createStreamManifest() {		
		$manifest = $GLOBALS['bizconf']->generateManifest();
		if (!empty($manifest)) {
			$license = $GLOBALS['bizconf']->getLicense();
			
			return array('manifest' => $manifest, 'license' => $license);
		}
		else {
			Logger::writelog(array('result_code' => 431, 'result' => 'LIVE_MAPPING_CONFLICT'));
			return null;
		}
	}
	
	// public static function createTestStreamPath() {		
		// $plurl = $GLOBALS['bizconf']->generatePlaylist();
		// if (!empty($plurl)) {
			// $qstring = $GLOBALS['bizconf']->getrsa256qstring();
			
			// return "$plurl?$qstring";
		// }
		// else {
			// Logger::writelog(array('result_code' => 431, 'result' => 'LIVE_MAPPING_CONFLICT'));
			// return null;
		// }
	// }
	
	// private static function isBlackout () {
		// $lists = $GLOBALS['redis']->getRedis("*;trueid;o070;*", 3);
		// if (count($lists) > 0) {
			// arsort($lists);
			// $tmstmp = time();
			// foreach ($lists as $list) {
				// $row = explode(";", $list);
				// if ($row[1] <= $tmstmp && $tmstmp <= $row[0]) return true;
			// }
			// return false;
		// }
		// else return false;
	// }
}