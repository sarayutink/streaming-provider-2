<?
    require_once __DIR__.'/vendor/autoload.php';
    use PhpAmqpLib\Connection\AMQPStreamConnection;
    use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQ{
    private static function getOrderIndex() {
		return array("uid", "sessionid", "appid", "channelid", "streamname", "langid", "streamlvl", "type", "stime", "duration", "csip", "agent", "visitor", "referer", "drm");
	}
    public static function write_to_rabbitmq($result_arr) {

        $connection = AMQPStreamConnection::create_connection([
            ['host' => HOST1, 'port' => PORT, 'user' => USER, 'password' => PASS, 'vhost' => VHOST],
            ['host' => HOST2, 'port' => PORT, 'user' => USER, 'password' => PASS, 'vhost' => VHOST]],
            []);
        $channel = $connection->channel();
        $channel->queue_declare(QUEUE, false, true, false, false);
        $channel->exchange_declare(EXCHANGE, 'topic', false, true, false);
        $channel->queue_bind(QUEUE, EXCHANGE, ROUTING_KEY);

        $cdr_arr['date'] = date("[d/M/Y:H:i:s O]");
		$cdr_arr['controller'] = $GLOBALS['ctrl_name'];
		$cdr_arr['desc'] = ($result_arr['result_code'] == 200 ? 'SUCCESS' : 'FAILED');
		$cdr_arr['result_code'] = $result_arr['result_code'];
		$cdr_arr['result'] = "\"".@$result_arr['result'].@$result_arr['streamurl']."\"";
		$cdr_arr['license'] = (isset($result_arr['license']) ? "\"".$result_arr['license']."\"" : "-");
		foreach (self::getOrderIndex() as $key) $cdr_arr[$key] = (!empty($GLOBALS['json_object'][$key]) ? $GLOBALS['json_object'][$key] : "-");
		$cdr_arr['csip'] = strrpos($cdr_arr['csip'], ".") ? $cdr_arr['csip'] : "-";
		$cdr_arr['agent'] = "\"".$cdr_arr['agent']."\"";        
        $log = implode('  ', $cdr_arr);
        $messageBody = $log."\n";
        $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
        $channel->basic_publish($message, EXCHANGE, ROUTING_KEY);
        $channel->close();
        $connection->close();
    }
}
