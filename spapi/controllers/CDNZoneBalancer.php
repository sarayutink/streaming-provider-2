<?
class CDNZoneBalancer {	
	public static function findServerByIP ($actionURL = "http://127.0.0.1:88/lernaean") {
		
		$param = array(
			"appid"     => $GLOBALS['json_object']['appid'],
			"channelid" => @$GLOBALS['json_object']['channelid'] . @$GLOBALS['json_object']['streamname'],
			"csip"      => $GLOBALS['json_object']['csip'],
			"type"      => $GLOBALS['json_object']['type'],
			"visitor"   => $GLOBALS['json_object']['visitor'],
			"streamlvl" => $GLOBALS['json_object']['streamlvl'],
			"charge"    => true,
			"drm"       => isset($GLOBALS['json_object']['drm']) ? $GLOBALS['json_object']['drm'] : "aes" 
		);

		require_once SRCDIR .'/libraries/Curlcstm.php';
		$result = Curlcstm::postJsonObj($actionURL, json_encode($param));
		if (strpos($result, "true") !== false) {
			return $result;
		}
		else {
			if ($result == "not found.") return serialize(array('result_code' => 421, 'result' => 'NO_CDN_IN_GORUP_CHAIN'));
			else return serialize(array('result_code' => 423, 'result' => 'LOAD_BALAN_NOT_RESPOND'));
		}
	}
}
?>