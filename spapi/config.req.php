<?
define("SRCDIR", __DIR__);
define("LOGDIR", "/www/logs/providers");

// define("SPRDSTGHOST", "10.18.12.100");
// define("SPRDSTGPORT", 6380);
define("SPRDSTGHOST", "sprdhost");
define("SPRDSTGPORT", 6380);
define("SPRDHOST", "sprdhost");
define("SPRDPORT", 6380);
global $sprdhost;
$sprdhost = "sprdhost";
global $lbrdhost;
$lbrdhost = "lbrdhost";
define("LBRDPORT", 6383);
define("LBQUOTA", 0.4);

define("BZRDHOST", "bzrdhost");
define("BZRDPORT", 6379);
define("BZENABLE", true);

require_once SRCDIR .'/libraries/Mwredis.php';
global $rd_3;
$rd_3 = new Mwredis(SPRDHOST);
global $rd_stg;
$rd_stg = new Mwredis(SPRDHOST);

require_once SRCDIR . '/amqplib/RabbitMQ.php';
define('HOST1', '10.18.12.63');
define('HOST2', '10.18.12.64');
define('PORT', 5672);
define('USER', 'syseng');
define('PASS', '123456');
define('VHOST', '/');
define('EXCHANGE', 'exchange.test');
define('QUEUE', 'queue.test');
define('ROUTING_KEY', 'routingkey.test');

require_once SRCDIR .'/models/Variable.php';
require_once SRCDIR .'/models/Logger.php';


$json_object = '';
$ctrl_name = '';
$config = '';
