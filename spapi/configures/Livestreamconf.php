<?
class Livestreamconf {
    
    public function overrideparams () {
        if ($GLOBALS['json_object']['drm'] == "wv1") $GLOBALS['json_object']['drm'] = "wv";
    }

	public function isBlacklist () {
        switch ($GLOBALS['rd_3']->getRedis($GLOBALS['json_object']['uid'], 7)) {
            case false :
                return false;
            break;
            case null :
                return false;
            break;
            default :
                return true;
            break;
        }
        // return !is_bool($GLOBALS['rd_3']->getRedis($GLOBALS['json_object']['uid'], 7));
	  }
	  
	public function isCCOver () {
		require_once SRCDIR .'/configures/Businesconf.php';
		$bizconf = new Businesconf();
		if (@$GLOBALS['json_object']['ccucheck'] === true) return $bizconf->isCCuOverGroup($GLOBALS['json_object']['uid'], $GLOBALS['json_object']['sessionid'], $GLOBALS['json_object']['channelid']);
		else return false;
  	}
    
    #"all;th;live;mobile;medium;wv;184"
	public function generatePlaylist () {
        $redis = new Redis();
        $return = array("result_code" => 433, "result" => "EMPTY_LIVE_MAPPING");
        try {
            $redis->connect(SPRDHOST, SPRDPORT);

            # isBlackout
            $this->isBlackout = false;
            $redis->select(3);
            $lists = $redis->keys("*".$GLOBALS['json_object']['appid'].";".$GLOBALS['json_object']['channelid']."*");
            if (count($lists) > 0) {
                arsort($lists);
                $tmstmp = time();
                foreach ($lists as $list) {
                    $row = explode(";", $list);
                    if ($row[1] <= $tmstmp && $tmstmp <= $row[0]) {
                        // var_dump($list);
                        $this->isBlackout = true;
                    }
                }
            }

            # mapping smil
            $query = !$this->isBlackout ? implode(";", array($GLOBALS['json_object']['appid'], $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], $GLOBALS['json_object']['visitor'], $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], $GLOBALS['json_object']['channelid'])) : "blackout";
            $default = !$this->isBlackout ? implode(";", array("all", $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], "all", $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], $GLOBALS['json_object']['channelid'])) : "blackout";
            $redis->select(1);
            $smils = $redis->mget([$query, $default]);
            if ($smils[0] !== false) $return = array("result_code" => 200, "result" => $smils[0]);
            else {
                if ($smils[1] !== false) $return = array("result_code" => 200, "result" => $smils[1]);
                else array("result_code" => 433, "result" => "EMPTY_LIVE_MAPPING");
            }

            $redis->close();
            
            if ($GLOBALS['json_object']['type'] == "catchup" && $return <> false) {
                if (strpos($return['result'], "yyyymmdd_HHmmss")) $return = str_replace("yyyymmdd_HHmmss", $this->changeDateFormat($GLOBALS['json_object']['stime'], "Ymd_His"), $return);
                if (strpos($return['result'], "yyyymmdd")) $return = str_replace("yyyymmdd", $this->changeDateFormat($GLOBALS['json_object']['stime'], "Ymd"), $return);
                else array("result_code" => 433, "result" => "EMPTY_LIVE_MAPPING");
            }
        }
		catch (Exception $e) {
            $return = array("result_code" => 439, "result" => "REDIS_LIVE_MAPPING_WENT_AWAY");
            file_put_contents(LOGDIR ."/generatePlaylist.err.log", date("Y/m/d_H:i:s") ."  ". SPRDHOST.":".SPRDPORT ."  ". $e->getMessage()."\n", FILE_APPEND);
        }
        $return['signature'] = $this->signQueryString(explode("/", $return['result'])[1]);

        return $return;
    }
	
	public function getrsaqstring ($bypass = false) {
        $drm = $GLOBALS['json_object']['drm'];
        if ($drm == "wv") $drm = "wv3";
        $channelid = $GLOBALS['json_object']['channelid'] == "en107" ? "107" : $GLOBALS['json_object']['channelid'];
        $channelid = !$this->isBlackout($GLOBALS['json_object']['appid'], $GLOBALS['json_object']['channelid']) ? explode("_", $channelid)[0] : "bk";
        $type = $GLOBALS['json_object']['type'] == "live" ? "live" : "dvr";
        $mpass = $bypass === false ? Opensslcryption::encrypt(time() ."|". $GLOBALS['json_object']['sessionid'] ."|". $GLOBALS['json_object']['appid'] ."|". $GLOBALS['json_object']['csip'] ."|". $channelid ."|". $GLOBALS['json_object']['uid'] ."|". $type) : Opensslcryption::encryptbypass();

		if ($GLOBALS['json_object']['type'] == "timeshift") @$querystring = "dvr=&";
		elseif ($GLOBALS['json_object']['type'] == "catchup") @$querystring = "dvr=&wowzadvrplayliststart={$this->changeDateFormat($GLOBALS['json_object']['stime'], "YmdHis")}&wowzadvrplaylistduration={$GLOBALS['json_object']['duration']}&";
		@$querystring .= "appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&drm={$drm}&uid={$GLOBALS['json_object']['uid']}&did=". str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']['sessionid'])) ."&mpass={$mpass}";
		
		return $querystring;
	}
	
	public function getdrmqstring () {
        $drm = $GLOBALS['json_object']['drm'];
        if ($drm == "wv") $drm = "wv3";
		$querystring = "appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&drm={$drm}&uid={$GLOBALS['json_object']['uid']}&did=". str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']['sessionid']));
		
		return $querystring;
	}
	
	public function changeDateFormat ($dateformat, $fromat) {
		return date($fromat, $dateformat);
    }
    
    public function isBlackout ($appid, $chid) {
		$lists = $GLOBALS['rd_3']->getRedis("*{$appid};{$chid}*", 3);
		if (count($lists) > 0) {
			arsort($lists);
			$tmstmp = time();
			foreach ($lists as $list) {
				$row = explode(";", $list);
				if ($row[1] <= $tmstmp && $tmstmp <= $row[0]) {
					// var_dump($list);
					return true;
				}
			}
			return false;
		}
		else return false;
	}
	
	public function signQueryString ($smil) {
		$secret = "ǝɯƖ┴ǝɔ@dSln0SʎʇƖl@ǝɹɹǝʍ0ԀpuƖW";
		$sid = bin2hex(openssl_random_pseudo_bytes(5));
		$rt = "".time();
		
		$payload = array($sid => array(
			$GLOBALS['json_object']['appid'], 
			$smil,
			$GLOBALS['json_object']['type'],
			$GLOBALS['json_object']['visitor'],
			"".$GLOBALS['json_object']['uid'],
			str_replace(array('+', '/', '='), array('-', '_' , ''), base64_encode($GLOBALS['json_object']['sessionid'])),
			$rt
		));
		
		$payloadKey = hash_hmac("sha256", $secret, $sid);
		
		$token = str_replace(array("+", "/", "="), array("-", "_", ""), base64_encode(hash_hmac("sha256", json_encode($payload), $payloadKey, true)));
		// file_put_contents("/www/logs/providers/milim.acc", json_encode(array("payload" => $payload, "payloadKey" => $payloadKey, "token" => $token)). "\n", FILE_APPEND);
		
		return "&sid={$sid}&rt={$rt}&tk={$token}";
	}
}
