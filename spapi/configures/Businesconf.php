<?
class Businesconf {
    private $contentGroup, $redis, $device, $isCCUOver;

	public function __Construct () {
		$this->redis = new Redis();
	}

    public function isBlacklist ($ssoid) {
        $exist = $this->redisCtrl(4, "exists", array($ssoid));
        return $exist;
    }

    public function isCCuOver ($ssoid, $device) {
        $return = false;
        if (BZENABLE) {
            try {
                $this->redis->connect(BZRDHOST, BZRDPORT);
                $this->redis->select(2);
                $white = $this->redis->exists($ssoid);
                if ($white == 0) {
                    $this->redis->select(3);
                    $device = count($this->redis->keys($ssoid."|".$device."|*"));
                    $ssoid = count($this->redis->keys($ssoid."|*"));
                    $this->redis->close();
                    if ($ssoid > 0) $return = ($device > 0 ? false : true);
                    else $return = false;
                }
                else {
                    $this->redis->close();
                    $return = false;
                }
            }
            catch (Exception $e) {
                // echo $e->getMessage() ."\n";
                newrelic_notice_error($e);
                return null;
            }
        }
        return $return;
    }

    public function isCCuOverGroup ($ssoid, $device, $content) {
        $this->isCCUOver = false;
        $this->device = $device;
        // if (BZENABLE) {
        if (false) {
            try {
                $this->redis = new Redis;
                $this->redis->connect(BZRDHOST, BZRDPORT);
                # check whitelist
                $this->redis->select(2);
                $white = $this->redis->exists($ssoid);
                if ($white == 0) {
                    # get current content group
                    $this->redis->select(5);
                    // echo $content;
                    $this->contentGroup = $this->redis->get($content);
                    if (!is_null($this->contentGroup)) {
                        # get all active devices
                        $this->redis->select(3);
                        // echo $ssoid."|*|".$this->contentGroup;
                        $alldevices = $this->redis->keys($ssoid."|*|".$this->contentGroup);
                        // var_dump($alldevices);
                        if (count($alldevices) > 0) {
                            $this->isCCUOver = true;
                            array_walk($alldevices, array($this,'mapUnderGroup'));
                        }
                    }
                    $this->redis->close();
                }
                else {
                    $this->redis->close();
                    $this->isCCUOver = false;
                }
            }
            catch (Exception $e) {
                echo $e->getMessage() ."\n";
            }
        }
        return $this->isCCUOver;
    }

    private function mapUnderGroup ($device) {
        $devicesegment = explode("|", $device);
        $this->redis->select(5);
        $ctnGrp = $this->redis->get($devicesegment[3]);
        if ($this->contentGroup === $ctnGrp) {
            // var_dump(is_int(strpos($device, "|".$this->device."|")));
            if (is_int(strpos($device, "|".$this->device."|"))) $this->isCCUOver = false;
        }
    }

    private function redisCtrl ($redisdb, $func, $params) {
        try {
            // $this->redis->connect("10.18.19.98", 6383);
            $this->redis->connect(BZRDHOST, BZRDPORT);
            $this->redis->select($redisdb);
            $return = $this->redis->$func(...$params);
            $this->redis->close();

            return $return;
        }
		catch (Exception $e) {
             echo $e->getMessage() ."\n";
            return null;
		}
    }
}
