<?
class Radiostreamconf {	
    public function generatePlaylist () {
        $redis = new Redis();
        $return = array("result_code" => 433, "result" => "EMPTY_LIVE_MAPPING");
        try {
            $redis->connect(SPRDHOST, SPRDPORT);
            $query = implode(";", array($GLOBALS['json_object']['appid'], $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], $GLOBALS['json_object']['visitor'], $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], $GLOBALS['json_object']['channelid']));
            $default = implode(";", array("all", $GLOBALS['json_object']['langid'], $GLOBALS['json_object']['type'], "mobile", $GLOBALS['json_object']['streamlvl'], $GLOBALS['json_object']['drm'], $GLOBALS['json_object']['channelid']));
            $redis->select(1);
            $smils = $redis->mget([$query, $default]);
            
            if ($smils[0] !== null){
                $return = array("result_code" => 200, "result" => $smils[0]);
            }else{
                if ($smils[1] !== null) {
                    $return = array("result_code" => 200, "result" => $smils[1]);
                }else{
                    array("result_code" => 433, "result" => "EMPTY_LIVE_MAPPING");
                }
            }
            $redis->close();

        }catch (Exception $e) {
            $return = array("result_code" => 439, "result" => "REDIS_LIVE_MAPPING_WENT_AWAY");
            file_put_contents(LOGDIR ."/generatePlaylist.err.log", date("Y/m/d_H:i:s") ."  ". SPRDHOST.":".SPRDPORT ."  ". $e->getMessage()."\n", FILE_APPEND);
        }
        return $return;
    }
    public function getrsaqstring ($bypass = false) {
        $mpass = $bypass === false ? Arsopensslcryption::encrypt(time() ."|". $GLOBALS['json_object']['sessionid'] ."|". $GLOBALS['json_object']['appid'] ."|". $GLOBALS['json_object']['csip'] ."|". $GLOBALS['json_object']['channelid'] ."|". $GLOBALS['json_object']['uid'] ."|". $GLOBALS['json_object']['type']) : Arsopensslcryption::encryptbypass();
        @$querystring .= "appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}&mpass={$mpass}";
        return $querystring;
    }
}
