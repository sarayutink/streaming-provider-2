<?
require_once __DIR__.'/config.req.php';

class Livestreamstg {
	public static function provide () {
		$GLOBALS['ctrl_name'] = "livestream";
		$GLOBALS['json_object'] = json_decode(file_get_contents("php://input"), true);
		## validate post json
		if (Variable::validate()) {
			require_once __DIR__."/Livestreamstg.req.php";
			$qstring = $GLOBALS['config']->getrsaqstring();
			$livemaping = $GLOBALS['config']->generatePlaylist();
			$is_livemaping = !is_bool($livemaping);
			$lb = new NewCDNZoneBalancer();
			$cdn = $lb->findCDNByIP();
			$is_cdn = is_int(strpos($cdn, "true"));

			switch($GLOBALS['json_object']['drm']) {
				case "aes":
					if ($is_livemaping && $is_cdn) {
						$protocol = "http";
						if (!in_array($GLOBALS['json_object']['channelid'], explode("|", "107|en107")) && in_array($GLOBALS['json_object']['appid'], explode("|", "trueid|trueidtv")) && in_array($GLOBALS['json_object']['visitor'], explode("|", "mobile")) && in_array($GLOBALS['json_object']['type'], explode("|", "live"))) $protocol = "https";
						$return['result_code'] = 200;
						$return['result'] = "$protocol://". $cdn ."/". $livemaping ."/playlist.m3u8?". $qstring;
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						$return['result'] = "Cannot find streaming server.";
						Logger::writelog(unserialize($cdn));
					}
					$return['version'] = "3.0";
				break;
				case "wv":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn ."/". $livemaping ."/manifest.mpd?appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}";
						$return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $qstring;
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						$return['result'] = "Cannot find streaming server.";
						Logger::writelog(unserialize($cdn));
					}
					$return['version'] = "3.0";
				break;
				case "fp":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn ."/". $livemaping ."/playlist.m3u8?appid={$GLOBALS['json_object']['appid']}&type={$GLOBALS['json_object']['type']}&visitor={$GLOBALS['json_object']['visitor']}&uid={$GLOBALS['json_object']['uid']}";
						$return['license'] = "https://kd.stm.trueid.net/scylla/drmdecrypt?". $qstring;
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						$return['result'] = "Cannot find streaming server.";
						Logger::writelog(unserialize($cdn));
					}
					$return['version'] = "3.0";
				break;
				default:
					$return = array('result_code' => 630, 'result' => "Invalid DRM request.");
					Logger::writelog($return);
				break;
			}
		}
		else {
			$return = array('result_code' => 600, 'result' => "Invalid request.");
			Logger::writelog($return);
		}
		
		
		return $return;
    }
}
