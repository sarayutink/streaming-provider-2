<?
require_once __DIR__.'/config.req.php';

class Radiostream {
    public static function provide () {
        $GLOBALS['ctrl_name'] = "radiostream";
        $GLOBALS['json_object'] = json_decode(file_get_contents("php://input"), true);
        require_once __DIR__."/Radiostream.req.php";
        $GLOBALS['json_object']['appid'] = "stg";
        ## validate post json
        if (Variable::validate()) {
            $qstring = $GLOBALS['config']->getrsaqstring();
            $livemaping = $GLOBALS['config']->generatePlaylist();
            $is_livemaping = ($livemaping['result_code'] == 200);
            $lb = new NewCDNZoneBalancer();
            $cdn = $lb->findCDNByIP();
            $is_cdn = ($cdn['result_code'] == 200);
            if($GLOBALS['json_object']['visitor'] == "wap") {
                if ($is_livemaping && $is_cdn) {
                    $protocol = "http";
                    $return['result_code'] = 200;
                    $return['result'] = "$protocol://". $cdn['result'] ."/". $livemaping['result'] ."/playlist.m3u8?". $qstring;
                    //Logger::writelog($return);
                }
                if (!$is_cdn) {
                    $return['result_code'] = $cdn['result_code'];
                    $return['result'] = "Cannot find streaming server.";
                    //Logger::writelog($cdn);
                }
                if (!$is_livemaping) {
                    $return['result_code'] = 430;
                    $return['result'] = "Cannot find playlist.";
                    //Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
                }
                $return['version'] = "3.0";
            }else{
                if ($is_livemaping && $is_cdn) {
                    $protocol = "https";
                    $return['result_code'] = 200;
                    $return['result'] = "$protocol://". $cdn['result'] ."/". $livemaping['result'] .".stream_acc?". $qstring;
                    Logger::writelog($return);
                }
                if (!$is_cdn) {
                    $return['result_code'] = $cdn['result_code'];
                    $return['result'] = "Cannot find streaming server.";
                    Logger::writelog($cdn);
                }
                if (!$is_livemaping) {
                    $return['result_code'] = 430;
                    $return['result'] = "Cannot find playlist.";
                    Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
                }
                $return['version'] = "3.0";
            }
        }else {
			$return = array('result_code' => 600, 'result' => "Invalid request.");
			//Logger::writelog($return);
        }
        return $return;
    }
}