<?
require_once __DIR__.'/config.req.php';

class Livestream {
	public static function provide () {
		$GLOBALS['ctrl_name'] = "livestream";
		$GLOBALS['json_object'] = json_decode(file_get_contents("php://input"), true);
		## validate post json
		require_once __DIR__."/Livestream.req.php";
		$isBlacklist = $GLOBALS['config']->isBlacklist();
		$isCCuOver = $GLOBALS['config']->isCCOver();
		if (Variable::validate() && !$isBlacklist && !$isCCuOver) {
			$GLOBALS['config']->overrideparams();
			$qstring = $GLOBALS['config']->getrsaqstring();
			// $GLOBALS['json_object']['appid'] = "antv1";
			$livemaping = $GLOBALS['config']->generatePlaylist();
			$is_livemaping = ($livemaping['result_code'] == 200);
			// $GLOBALS['json_object']['appid'] = "stg";
			$lb = new NewCDNZoneBalancer();
			$cdn = $lb->findCDNByIP();
			$is_cdn = ($cdn['result_code'] == 200);

			switch($GLOBALS['json_object']['drm']) {
				case "aes":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						if (in_array($GLOBALS['json_object']['visitor'], explode("|", "web|websport|lwweb|mobile|chromecast|tv"))) {
							$protocol = "https";
							$return['result'] = "$protocol://". $cdn['result'] ."/". $livemaping['result'] ."/playlist.m3u8?". $qstring . $livemaping['signature'];
						}
						else  {
							$protocol = "rtsp";
							$return['result'] = "$protocol://". $cdn['result'] ."/". $livemaping['result'] ."?". $qstring . $livemaping['signature'];
						}
						Logger::writelog($return);
						//RabbitMQ::write_to_rabbitmq($return);
					}
					if (!$is_cdn) {
						$return['result_code'] = $cdn['result_code'];
						$return['result'] = "Cannot find streaming server.";
						Logger::writelog($cdn);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
					}
					$return['version'] = "3.0";
				break;
				case "wv":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn['result'] ."/". $livemaping['result'] ."/manifest.mpd?". $GLOBALS['config']->getdrmqstring() . $livemaping['signature'];
						$return['license'] = "https://kd-preprod.stm.trueid.net/search?". $qstring . $livemaping['signature'];
						// $return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $qstring . $livemaping['signature'];
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						$return['result'] = "Cannot find streaming server.";
						Logger::writelog(unserialize($cdn));
					}
					$return['version'] = "3.0";
				break;
				case "wv1":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn['result'] ."/". $livemaping['result'] ."/manifest.mpd?". $GLOBALS['config']->getdrmqstring() . $livemaping['signature'];
						$return['license'] = "https://kd-preprod.stm.trueid.net/search?". $qstring . $livemaping['signature'];
						// $return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $qstring . $livemaping['signature'];
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						$return['result'] = "Cannot find streaming server.";
						Logger::writelog(unserialize($cdn));
					}
					$return['version'] = "3.0";
				break;
				case "fp":
					if ($is_livemaping && $is_cdn) {
						$return['result_code'] = 200;
						$return['streamurl'] = "https://". $cdn['result'] ."/". $livemaping['result'] ."/playlist.m3u8?". $GLOBALS['config']->getdrmqstring() . $livemaping['signature'];
						$return['license'] = "https://kd-preprod.stm.trueid.net/fruit?". $qstring . $livemaping['signature'];
						// $return['license'] = "https://kd.stm.trueid.net/scylla/newdrmdecrypt?". $qstring . $livemaping['signature'];
						Logger::writelog($return);
					}
					if (!$is_livemaping) {
						$return['result_code'] = 430;
						$return['result'] = "Cannot find playlist.";
						Logger::writelog(array("result_code" => 433, "result" => "NOT_EXIST_LIVE_MAPPING"));
					}
					if (!$is_cdn) {
						$return['result_code'] = 420;
						$return['result'] = "Cannot find streaming server.";
						Logger::writelog(unserialize($cdn));
					}
					$return['version'] = "3.0";
				break;
				default:
					$return = array('result_code' => 630, 'result' => "Invalid DRM request.");
					Logger::writelog($return);
				break;
			}
		}
		elseif ($isBlacklist) {
			switch($GLOBALS['json_object']['drm']) {
				case "aes" :
					$return = array('result_code' => 210, 'result' => "https://thumbnail.stm.trueid.net/blacklist/playlist.m3u8?".$GLOBALS['config']->getrsaqstring());
				break;
				case "wv" :
					$return['result_code'] = 210;
					$return['streamurl'] = "https://thumbnail.stm.trueid.net/blacklistMpd/manifest.mpd?". $GLOBALS['config']->getdrmqstring();
					$return['license'] = "https://kd-preprod.stm.trueid.net/search?". $GLOBALS['config']->getrsaqstring();
					// $return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $GLOBALS['config']->getrsaqstring();
				break;
				case "wv1" :
					$return['result_code'] = 210;
					$return['streamurl'] = "https://thumbnail.stm.trueid.net/blacklistMpd/manifest.mpd?". $GLOBALS['config']->getdrmqstring();
					$return['license'] = "https://kd-preprod.stm.trueid.net/search?". $GLOBALS['config']->getrsaqstring();
					// $return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $GLOBALS['config']->getrsaqstring();
				break;
				case "fp" :
					$return['result_code'] = 210;
					$return['streamurl'] = "https://thumbnail.stm.trueid.net/blacklistHls/playlist.m3u8?". $GLOBALS['config']->getdrmqstring();
					$return['license'] = "https://kd-preprod.stm.trueid.net/fruit?". $GLOBALS['config']->getrsaqstring();
					// $return['license'] = "https://kd.stm.trueid.net/scylla/newdrmdecrypt?". $GLOBALS['config']->getrsaqstring();
				break;
				default :
					$return = array('result_code' => 210, 'result' => "https://thumbnail.stm.trueid.net/blacklist/playlist.m3u8");
				break;
			}
			Logger::writelog($return);
		}
		elseif ($isCCuOver) {
			switch($GLOBALS['json_object']['drm']) {
				case "aes" :
					$return = array('result_code' => 220, 'result' => "https://thumbnail.stm.trueid.net/ccu_limitation/playlist.m3u8?".$GLOBALS['config']->getrsaqstring());
				break;
				case "wv" :
					$return['result_code'] = 220;
					$return['streamurl'] = "https://thumbnail.stm.trueid.net/CCULOCKMPD/manifest.mpd?". $GLOBALS['config']->getdrmqstring();
					$return['license'] = "https://kd-preprod.stm.trueid.net/search?". $GLOBALS['config']->getrsaqstring();
					// $return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $GLOBALS['config']->getrsaqstring();
				break;
				case "wv1" :
					$return['result_code'] = 220;
					$return['streamurl'] = "https://thumbnail.stm.trueid.net/CCULOCKMPD/manifest.mpd?". $GLOBALS['config']->getdrmqstring();
					$return['license'] = "https://kd-preprod.stm.trueid.net/search?". $GLOBALS['config']->getrsaqstring();
					// $return['license'] = "https://kd.stm.trueid.net/charybdis/drmdecrypt?". $GLOBALS['config']->getrsaqstring();
				break;
				case "fp" :
					$return['result_code'] = 220;
					$return['streamurl'] = "https://thumbnail.stm.trueid.net/ccuLimitationHls/playlist.m3u8?". $GLOBALS['config']->getdrmqstring();
					$return['license'] = "https://kd-preprod.stm.trueid.net/fruit?". $GLOBALS['config']->getrsaqstring();
					// $return['license'] = "https://kd.stm.trueid.net/scylla/newdrmdecrypt?". $GLOBALS['config']->getrsaqstring();
				break;
				default :
					$return = array('result_code' => 220, 'result' => "https://thumbnail.stm.trueid.net/ccu_limitation/playlist.m3u8");
				break;
			}
			Logger::writelog($return);
		}
		else {
			$return = array('result_code' => 600, 'result' => "Invalid request.");
			Logger::writelog($return);
		}
		
		
		return $return;
    }
	
	private static function variable_exception () {
		$appid_exc = !in_array($GLOBALS['json_object']['appid'], array("antvhb1"));
		return $appid_exc;
	}
}
