<?php
// side effect
ini_set('error_reporting', E_ERROR);

function main ($json) {
	$redis_arr = array(
		6379 => "bzrdhost",
		6380 => "sprdhost",
		6383 => "lbrdhost"
	);
	// writeLogs("param", $json ."\n", FILE_APPEND);
  
	$result = true;
	$rdports = array();
	foreach ($redis_arr as $port => $host) {
		$redis_result = testConn($host, $port);
		if ($redis_result == false) $rdports[] = $port;
		$result = $result & $redis_result;
	}

	if ($result != 1) {
		$msg = "[". gethostname(). "] service redis port [". implode(", ", $rdports) ."] failed.";
		notify("warning", $msg);
		echo "fail";
		header("HTTP/1.0 520 Cache not found.");
	}
	else echo "success";
}

function testConn ($host, $port) {
	$redis = new Redis();
	try {
		$conn = $redis->connect($host, $port);
		$redis->close();

		return $conn;
	}
	catch (Exception $e) {
		return false;
	}
}

function notify ($mode, $details) {
    date_default_timezone_set("Asia/Bangkok");
    $timestamp = date("Y:m:d-H:i:s");
    if (is_array($details)) $details = implode("  ", $details);
    $details = $timestamp ."  ". __FILE__ ."  ". $details;
  
    $sToken = "gbaX513e4nUrGQu3QAiFONd67gSx1X8pOOKewt9HdfS";

    $chOne = curl_init(); 
    curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
    curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt( $chOne, CURLOPT_PROXY, "10.18.19.42:80");
    curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt( $chOne, CURLOPT_POST, 1);
    curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$details);
    $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
    curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec( $chOne );

    //Result error
    //if(curl_error($chOne)) file_put_contents(__DIR__."/logs/cdnpool-notify-err_".date("Ymd_H").".log", date("Y/m/d H:i:s")."  ".'error:' . curl_error($chOne)."\n", FILE_APPEND);
    curl_close( $chOne );
}

main();
?>